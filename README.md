# TODO app

Cílem projektu je jednoduchý TODO maker, umožňující ukládát si úkoly jednoduše a přiložit k nim další inforamace, jakožto filtry, místo vytvoření a čas, kdy by se úkol měl odehrávat.

## **Popis a postup**

---

1. **K implmentaci byly použity technologie:**

    - Html
    - CSS
    - Vanilla JS

2. **Aplikace se skládá z 3 hlavních částí**

    - _Hlavního Dashboardu_

        - Obsahuje především jednotlivé tasky

    - _Přídání nového tasku_

        - Obsahuje zejména jednotlivé pole na detaily

    - _Detail jednotlivých tasků_
        - Zobrazuje detaily tasku a umožnuje i změnit obsažený text

## Poznámky

---

Čistě z neformalní stránky, aplikace byla napsána trochu hekticky z důvodu zvláštních přístupů jiných předmětů, dále byla snaha spnit co nejvíce kritérii, teré jsou někdy implementovány opravdu ne úplně vhodně, jelikož se k projektu vůbec nehodí, ovšem o body bych se nerad ošidil.

### Vědomě nesplněné požadavky

-   Použití JS frameworku

    -   Důvodem je časová náročnost a komplexita, jelikož i vanilla JS byla pro mě úplná novinka.

-   Mimo kritéria, mi validátor tvrdí že input typu time, je nevhodný, ale podle caniuse by Chrome ani Mozilla nemeli mít problém a pro ty by měla být implementace primárně, tak věřím, že je to dostačující.
