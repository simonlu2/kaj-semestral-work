//  Main class for logic
const includeDummyData = true;
const errorSound = new Audio("media/error.mp3");

class Controller {
	filters = ["all", "personal", "business", "sport", "school"];

	dummyData = [
		{
			id: "id" + -1,
			type: "personal",
			title: "Examle Title",
			time: "15:30",
			address: "Vodickova, Praha",
			text: "Dummy random text...",
		},
		{
			id: "id" + -2,
			type: "business",
			title: "business Title",
			time: "15:30",
			address: "Zemedelska, Praha",
			text: "Dummy another random text...",
		},
		{
			id: "id" + -3,
			type: "sport",
			title: "sport Title",
			time: "15:30",
			address: "Zelena, Praha",
			text: "Dummy more random text...",
		},
		{
			id: "id" + -4,
			type: "school",
			title: "school Title",
			time: "15:30",
			address: "Myslbeckova, Praha",
			text: "Dummy even more random text...",
		},
	];

	constructor() {
		const date = new Date().toLocaleDateString();
		document.getElementById("headerDate").innerHTML = date;

		//uncomment to drop all the data from storage
		//this.dropData();

		this.storage = JSON.parse(window.localStorage.getItem("data")) || [];

		if (includeDummyData && this.storage.length == 0) {
			//whether the dummy data should be included
			for (const element of this.dummyData) {
				this.storage.push(element);
			}
		}

		document
			.getElementById("submit")
			.addEventListener("click", () => this.submitted(this));

		document
			.getElementById("trash")
			.addEventListener("click", () => this.delete());

		document
			.getElementById("edit")
			.addEventListener("click", () => this.saveState());

		document
			.getElementById("navContainer")
			.addEventListener("click", () =>
				this.applyFilter(
					document.querySelector('input[name="rNav"]:checked')
				)
			);

		document
			.querySelector(".fa-map-marker-alt")
			.addEventListener("click", () => this.findMe());

		this.renderItems();

		this.initGeo();
	}

	dropData() {
		// drops data from the storage
		window.localStorage.setItem("data", null);
	}

	initGeo() {
		// initializing provider of location information
		this.reverseGeocoder = new BDCReverseGeocode();
		this.reverseGeocoder.localityLanguage = "en";
	}

	renderItems() {
		// render all the elements in the storage - all the tasks
		document.getElementById("listContainer").innerHTML = "";
		document.getElementById("total").innerHTML = this.storage.length;
		for (const element of this.storage) {
			renderer.showElement(element);
		}
	}

	applyFilter(filter) {
		// filters data acording to selected filter
		if (!filter) return;
		document.getElementById("listContainer").innerHTML = "";
		this.storage.forEach((element) => {
			if (element.type == filter.value || filter.value == "all") {
				renderer.showElement(element);
			}
		});
	}

	findMe() {
		// get user's location
		if (!navigator.geolocation) {
			alert("Geolocation is not supported by your browser");
		} else {
			navigator.geolocation.getCurrentPosition((position) => {
				this.reverseGeocoder.getClientLocation(
					{
						latitude: position.coords.latitude,
						longitude: position.coords.longitude,
					},
					(result) => {
						renderer.showLocation(
							result.countryCode +
								" " +
								result.principalSubdivision +
								result.locality
						);
					}
				);
			}, this.error);
		}
	}

	error() {
		alert("Unable to retrieve your location");
	}

	saveStorage() {
		// puts local data to window.localstorage
		this.dropData();
		window.localStorage.setItem("data", JSON.stringify(this.storage));
	}

	submitted(root) {
		// new task created, capturing data
		let types = document.querySelector('input[name="aType"]:checked');

		let title = document.getElementById("currentTitle").value;
		let type = types ? types.value : "";
		let time = document.getElementById("time").value;
		let address = document.getElementById("address").value;
		let text = document.getElementById("text").value;

		let container = {
			id: "id" + this.storage.length,
			type: type,
			title: title,
			time: time,
			address: address,
			text: text,
		};
		root.validate(container);
	}

	validate(element) {
		// validating user's input
		for (const [key, value] of Object.entries(element)) {
			if (value == "") {
				errorSound.play();
				this.raiseAlert(key);
				return;
			}
		}
		document.getElementById("mario").play();
		this.createNew(element);
	}

	clearForm() {
		// resets the new task form
		document.getElementById("currentTitle").value = "";
		document.getElementById("time").value = "";
		document.getElementById("address").value = "";
		document.getElementById("text").value = "";
		document.getElementById("typeText").innerHTML = "Type";
	}

	createNew(element) {
		// creating new task
		this.storage.push(element);
		this.saveStorage();
		renderer.showMain();
		renderer.showElement(element);
		this.clearForm();
		document.getElementById("total").innerHTML = this.storage.length;
	}

	raiseAlert(field) {
		let msg = "Fill " + field + " please.";
		alert(msg);
	}

	delete() {
		// deleting a task
		document.getElementById("swoosh").play();
		const el = renderer.getElementFromID(renderer.id);
		const index = this.storage.indexOf(el);
		if (index > -1) this.storage.splice(index, 1);
		else {
			console.log(
				"error, deleting item, but it seems not to be inside the repo"
			);
			return;
		}
		this.saveStorage();
		renderer.hideDetail();
		this.renderItems();
	}

	saveState() {
		// save new state of the task
		document.getElementById("success").play();
		let newText = document.getElementById("activityText").innerHTML;
		let element = renderer.getElementFromID(renderer.id);
		element.text = newText;
		renderer.hideDetail();
	}
}

// main rendering class

class Renderer {
	// type related icons
	icons = {
		personal: "fa-user",
		business: "fa-briefcase",
		sport: "fa-running",
		school: "fa-user-graduate",
	};

	constructor() {
		this.setListeners();
	}

	setListeners() {
		// set all listeners
		document
			.getElementById("barContainer")
			.addEventListener("click", this.openNavbar);

		document
			.getElementById("navContainer")
			.addEventListener("click", this.showBar);

		document
			.querySelectorAll(".fa-play")
			.forEach((element) =>
				element.addEventListener("click", (e) => this.showDetail(e))
			);

		document
			.getElementById("arrow")
			.addEventListener("click", this.hideDetail);

		document
			.getElementById("type")
			.addEventListener("click", this.chooseType);

		document
			.querySelectorAll(".typeElement")
			.forEach((element) =>
				element.addEventListener("click", () => this.closeType())
			);

		document
			.getElementById("addButton")
			.addEventListener("click", this.newActivity);

		document
			.getElementById("arrowHolder")
			.addEventListener("click", this.showMain);

		document
			.getElementById("moon")
			.addEventListener("click", this.setNight);

		document.getElementById("sun").addEventListener("click", this.setDay);
	}

	setNight() {
		// dark mode
		document.getElementById("moon").style.display = "none";
		document.getElementById("sun").style.display = "flex";
		document.querySelector("body").style.background = "#2d3436";
		document.getElementById("bodyContainer").style.background = "#34495e";
		document.getElementById("bodyContainer").style.color = "white";
		ball.border = "white";
		ball.bounce();
	}

	setDay() {
		// light mode
		document.getElementById("moon").style.display = "flex";
		document.getElementById("sun").style.display = "none";
		document.querySelector("body").style.background = "#ced6e0";
		document.getElementById("bodyContainer").style.background = "white";
		document.getElementById("bodyContainer").style.color = "black";
		ball.border = "black";
		ball.bounce();
	}

	// returns the icon based on input
	getIcon(icon) {
		if (icon == "personal") return this.icons.personal;
		if (icon == "business") return this.icons.business;
		if (icon == "sport") return this.icons.sport;
		if (icon == "school") return this.icons.school;
	}

	showLocation(location) {
		// gets user's location and display in the coresponding field
		console.log(location);
		document.getElementById("address").value = location;
	}

	showElement(element) {
		// render the task
		let listElement = document.createElement("li");
		listElement.classList.add("listElement");

		let iconContainer = document.createElement("div");
		iconContainer.classList.add("iconContainer");

		let title = document.createElement("span");
		title.innerHTML = element.title;
		title.classList.add("listElementTitle");

		let address = document.createElement("span");
		address.innerHTML = element.address;
		address.classList.add("listElementAddress");

		let time = document.createElement("span");
		time.innerHTML = element.time;
		time.classList.add("listElementTime");

		let icon = document.createElement("i");
		icon.classList.add("fas", this.getIcon(element.type));

		let play = document.createElement("i");
		play.classList.add("fas", "fa-play");
		if (element.id) {
			play.setAttribute("id", element.id);
		}

		iconContainer.appendChild(icon);
		listElement.appendChild(iconContainer);
		listElement.appendChild(title);
		listElement.appendChild(address);
		listElement.appendChild(time);
		listElement.appendChild(play);

		document.getElementById("listContainer").appendChild(listElement);

		this.setListeners();
	}

	openNavbar() {
		let title = document.getElementById("title");
		let nav = document.getElementById("navContainer");
		let bar = document.getElementById("barContainer");
		title.style.left = "17%";
		bar.style.display = "none";
		nav.style.display = "block";
	}

	showBar() {
		// closing the bar and showing only the bar icon
		let title = document.getElementById("title");
		let nav = document.getElementById("navContainer");
		let bar = document.getElementById("barContainer");
		title.style.left = "0%";
		bar.style.display = "block";
		nav.style.display = "none";
	}

	newActivity() {
		// opens the new activity page
		let mainPage = document.getElementById("mainPage");
		let newActivity = document.getElementById("newActivity");
		mainPage.style.display = "none";
		newActivity.style.display = "block";
		history.pushState(null, null, "index.html#newActivity");
	}

	showMain() {
		// shows the main page
		let mainPage = document.getElementById("mainPage");
		let newActivity = document.getElementById("newActivity");
		mainPage.style.display = "block";
		newActivity.style.display = "none";
		history.pushState(null, null, "index.html#mainPage");
	}

	showDetail(e) {
		// show the detail page
		let detail = document.getElementById("detail");
		let body = document.getElementById("bodyContainer");
		let button = document.getElementById("addButton");

		body.style.display = "none";
		button.style.display = "none";
		detail.style.display = "grid";

		const element = this.getElementFromID(e.target.id);
		this.renderDetail(element);

		history.pushState(null, null, "index.html#detail");
	}

	getElementFromID(id) {
		// returns element from id
		let storage = controller.storage;

		for (const element of storage) {
			if (element.id == id) {
				return element;
			}
		}
	}

	renderDetail(element) {
		// render task details into coresponding fields
		document.getElementById("activityTitle").innerHTML = element.title;
		document.getElementById("activityAddress").innerHTML = element.address;
		document.getElementById("activityTime").innerHTML = element.time;
		document.getElementById("activityType").innerHTML = element.type;
		document.getElementById("activityText").innerHTML = element.text;
		this.id = element.id;
	}

	hideDetail() {
		// hides the detail screen
		let detail = document.getElementById("detail");
		let body = document.getElementById("bodyContainer");
		let button = document.getElementById("addButton");

		body.style.display = "block";
		button.style.display = "flex";
		detail.style.display = "none";

		history.pushState(null, null, "index.html#mainPage");
	}

	chooseType() {
		// shows types of tasks
		document.getElementById("map").style.display = "none";
		let types = document.getElementById("types");
		let address = document.getElementById("address");
		let time = document.getElementById("time");
		let text = document.getElementById("text");
		let submit = document.getElementById("submit");
		types.style.display = "flex";
		address.style.top = "13%";
		time.style.top = "13%";
		text.style.top = "13%";
		submit.style.top = "13%";
	}

	closeType() {
		// hides selector of tasks
		document.getElementById("map").style.display = "block";
		let child = document.getElementById("typeText");
		let input = document.querySelector('input[name="aType"]:checked');
		child.innerHTML = input ? input.value : "Type";
		let types = document.getElementById("types");
		let address = document.getElementById("address");
		let time = document.getElementById("time");
		let text = document.getElementById("text");
		let submit = document.getElementById("submit");
		types.style.display = "none";
		address.style.top = "0%";
		time.style.top = "0%";
		text.style.top = "0%";
		submit.style.top = "0%";
	}
}

class Ball {
	// Just a little ball bouncing on the screen, because I had no idea how to include svg to get the points I need
	constructor() {
		this.circle = document.createElementNS(
			"http://www.w3.org/2000/svg",
			"circle"
		);
		this.border = "black";
		this.circle.setAttributeNS(null, "cx", 100);
		this.circle.setAttributeNS(null, "cy", 30);
		this.circle.setAttributeNS(null, "r", "10");
		this.circle.setAttributeNS(null, "fill", "black");
		this.circle.setAttributeNS(null, "stroke", this.border);
		this.circle.setAttributeNS(null, "stroke-width", "2px");
		document.querySelector("#svgContainer").appendChild(this.circle);

		this.circle.addEventListener("mouseover", () => this.bounce());
	}

	bounce() {
		const WIDTH = document
			.getElementById("svgContainer")
			.getBoundingClientRect().width;
		let x = Math.floor(Math.random() * WIDTH) - 5;
		if (x <= 100) x = 150;
		const color = "#" + (((1 << 24) * Math.random()) | 0).toString(16);
		this.circle.setAttributeNS(null, "cx", x);
		this.circle.setAttributeNS(null, "fill", color);
		this.circle.setAttributeNS(null, "stroke", this.border);
	}
}
let renderer;
let controller;
let ball;
history.pushState(null, null, "index.html#mainPage");

window.onpopstate = (event) => {
	console.log(location.hash);
	const hash = location.hash;
	if (hash == "#mainPage") renderer.showMain();
	if (hash == "#detail") renderer.showDetail();
	if (hash == "#newActivity") renderer.newActivity();
};

window.onload = () => {
	renderer = new Renderer();
	controller = new Controller();
	ball = new Ball();
};
